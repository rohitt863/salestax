class TaxStretegy{
	getTax(countryData,selectedTax){
		var result=Constants.getZero();
   	    let country=countryData.toLowerCase();
   	    switch(country){
      	    case Constants.getIndia() :let indiaTaxStretegyObj=new IndiaTaxStretegy(selectedTax);
      	                 result=indiaTaxStretegyObj.indiaTax();
                		 break;
       	    case Constants.getUs() :let japanTaxStretegyObj=new JapanTaxStretegy(selectedTax);
       	    			  result=japanTaxStretegyObj.japanTax();
                		  break;
        	  case Constants.getJapan() :  let usTaxStretegyObj=new UsTaxStretegy(selectedTax);
        				 result=usTaxStretegyObj.usTax();
               			 break;    
        	default: result=5;
    	}
    	return result;
	}
}