	var firebaseConfig = {
	    apiKey: "AIzaSyCN5TU7VI_m1HjPgY6F9_mC4iMUirUPM7U",
	    authDomain: "tax-application-e826b.firebaseapp.com",
	    databaseURL: "https://tax-application-e826b.firebaseio.com",
	    projectId: "tax-application-e826b",
	    storageBucket: "",
	    messagingSenderId: "233957734257",
	    appId: "1:233957734257:web:49fcdc318bca3b2c"
	}; 

    firebase.initializeApp(firebaseConfig);
    var provider = new firebase.auth.FacebookAuthProvider();
	function fbSignIn(){
		firebase.auth().signInWithPopup(provider).then(function(result) {
		    // This gives you a Facebook Access Token. You can use it to access the Facebook API.
			var token = result.credential.accessToken;
			// The signed-in user info.
		    var user = result.user;
		    sessionStorage.setItem("token",Math.random());
		    window.location.href="processData.html";
		}).catch(function(error) {
				  // Handle Errors here.
			var errorCode = error.code;
		    var errorMessage = error.message;
			// The email of the user's account used.
			var email = error.email;
			// The firebase.auth.AuthCredential type that was used.
			var credential = error.credential;
		});
	}
       
	function fbSignOut(){
	   firebase.auth().signOut().then(function() {
		}).catch(function(error) {
  		// An error happened.
		});
	}
	var googleProvider = new firebase.auth.GoogleAuthProvider();
	function googleSignIn(){
		firebase.auth().signInWithPopup(googleProvider).then(function(result) {
		    // This gives you a Google Access Token. You can use it to access the Google API.
		    var token = result.credential.accessToken;
		    // The signed-in user info.
		    console.log("write");
		    var user = result.user;
		    sessionStorage.setItem("token",Math.random());
		    window.location.href="processData.html";
		}).catch(function(error) {
			// Handle Errors here.
			var errorCode = error.code;
			var errorMessage = error.message;
			// The email of the user's account used.
			var email = error.email;
			// The firebase.auth.AuthCredential type that was used.
			var credential = error.credential;
		});
	}

	function googleSignOut(){
		firebase.auth().signOut().then(function() {
 			 // Sign-out successful.
		}).catch(function(error) {
  			// An error happened.
		});
	}