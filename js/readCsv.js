sessionStorage.setItem("is_reloaded", true);
//sessionStorage.removeItem('columnLength');
sessionStorage.removeItem("selected");
var dataArray = [];
async function Upload() {
	var fileUpload = document.getElementById("fileUpload");
	var regex = /^([a-zA-Z0-9\s_\\.\-:])/;
  //check file uploaded or not
  let fileFoundOrNot= fileUploadedOrNot(regex);
  if(fileFoundOrNot==true){
    let extension = /(.csv)$/;
    //check file extension csv or not
    let extensionResult=checkExtension(extension);  
    if(extensionResult==true){
      try{
        //read data from file in array
        let lineOfArray=await readFileInArray();
        //check file is empty or not
        let checkFileEmptyResult=isEmpty(lineOfArray);
          if(checkFileEmptyResult==true){
            let headerResult= isHeaderValid(lineOfArray);
            if(headerResult==true){
              sessionStorage.setItem("selected",Constants.getSales());
              //count for checking another csv isAdded for appending in exisiting csv
              let count=clickCount();
              if(count>1){
                showNextCsvDataInTable(lineOfArray);                                          
              }else{
                //show data in table
                showDataInTable(lineOfArray);
              }
              sessionStorage.setItem("dataArray",JSON.stringify(dataArray));
              buttonActions();
            }
          }
      }catch(err){
        document.write(err);
      }
    }
  }                     
}

function fileUploadedOrNot(regex){
  if (regex.test(fileUpload.value.toLowerCase())) {
    return true;
  } else {
    alert(Constants.getChooseCsvFile());
    return false;
  }
}

function checkExtension(extension) {
  if (extension.test(fileUpload.value.toLowerCase())) {
    return true;        
  } else {
    alert(Constants.getInvalidExtension());
    return false;
  }
}

function readFileInArray(){
  return new Promise((resolve, reject)=>{
    if (typeof (FileReader) != "undefined") {
      var reader = new FileReader();
      reader.onload = function(event) {
        let lineInsertInArray=[];
        let readCsvLineByLine= event.target.result.split("\n"); 
        readCsvLineByLine=readCsvLineByLine.slice(0,readCsvLineByLine.length-1);
        readCsvLineByLine.forEach(function(item){
          let cells = item.split(Constants.getComma());
          lineInsertInArray.push(cells);  
        });
        resolve(lineInsertInArray); 
      } 
      reader.readAsText(fileUpload.files[0]);           
    } else {
      alert(Constants.getBrowserNotSupport());
    }
  });    
}

function isEmpty(rows){
  if(rows.length>1){
     return true;
  }else{
    alert(Constants.getCsvEmpty());
     return false;
  }   
}

function isHeaderValid(lineOfArray){
  let costIndex=-1;
  let countryIndex=-1;
  let header = lineOfArray[0];
  for(let index=0;index<header.length;index++){
    if(header[index].toLowerCase().trim()=="cost"){
       costIndex=index;
    }else if(header[index].toLowerCase().trim()=="country"){
      countryIndex=index;
    }
  }
  if(costIndex!=-1 && countryIndex!=-1){
    return true;
  }else{
    let msg=Constants.getHeaderNotSupported();
    alert(Constants.getColumnNotSupport());
    document.getElementById("coloumError").innerHTML=`<strong> ${msg}</strong>`;
    document.getElementById("coloumError").style.visibility="visible";
    return false;
  }
}

function clickCount(){
  if(typeof(Storage) !== "undefined") {
    if (sessionStorage.clickcount) {
      return sessionStorage.clickcount = Number(sessionStorage.clickcount)+1;
    } else {
      return sessionStorage.clickcount = 1;
    }
  }
}

function showNextCsvDataInTable(lineOfArray){
  if(lineOfArray[0].length==sessionStorage.getItem('columnLength')){
    addDataInPreviousArray(lineOfArray);
  }else{
    if(sessionStorage.getItem('columnLength')!=null){
      let columnResult=confirm(`Previous csv had column count:${sessionStorage.getItem('columnLength')},if you continue then you will lose previous data else you can choose the csv with same column count` );
      if(columnResult){
        sessionStorage.removeItem("dataArray");
        sessionStorage.setItem('columnLength',lineOfArray[0].length);
        showNextCsvDataInTable(lineOfArray);
      } 
    }else{
      //sessionStorage.removeItem("dataArray");
      sessionStorage.setItem('columnLength',lineOfArray[0].length);
      showNextCsvDataInTable(lineOfArray);
    }      
  }  
}

function addDataInPreviousArray(lineOfArray){
  if(sessionStorage.getItem("is_reloaded")){
    isSessionRefrece();
  }else{
    if ($.fn.DataTable.isDataTable("#input-table")) {
      $("#input-table").DataTable().destroy();
      $('#input-table tbody').empty();
    }
  }
  dataArray=JSON.parse(sessionStorage.getItem("dataArray"));
  let dataPushArry=[];
  dataPushArry=lineOfArray.slice(1,lineOfArray.length);
  if(dataArray==null){
    dataArray=[];
    dataPushArry=lineOfArray;
  }
  dataPushArry.forEach(function(item){
    dataArray.push(item); 
  });    
  document.getElementById('calculate').style.display='flex';   
  checkHeader();
  inputDataTable();  
}

function showDataInTable(lineOfArray){
  sessionStorage.removeItem("is_reloaded"); 
  var len = lineOfArray[0].length;
  sessionStorage.setItem('columnLength',len);
  headerForTable(lineOfArray[0]);
  dataArray=lineOfArray;
  document.getElementById('calculate').style.display='flex';
  inputDataTable();
}

function headerForTable(cells){
  let inputHeader="";
  for(let j=0;j<cells.length;j++){
    inputHeader+=`<th>${cells[j]}</th>`
  }
  document.getElementById("input-header").innerHTML=inputHeader;
}

function enableTrue(){
  document.getElementById("upload").disabled = false;
}

function inputDataTable(){
	$(document).ready(function(){
		$("#input-table").DataTable({
			data:dataArray.slice(1,dataArray.length),
			ordering:true,
			paging:true
		})
	});
}

function isSessionRefrece(){
  //sessionStorage.removeItem("dataArray");
  sessionStorage.removeItem("is_reloaded"); 
}

function checkHeader(){
  if(document.getElementById("input-header").value==undefined){
    let cell=dataArray[0];
    let headerInput="";
    cell.forEach(function(item){
      headerInput+=`<th>${item}</th>`
    });
    document.getElementById("input-header").innerHTML=headerInput;
  }
}

function buttonActions(){
  document.getElementById("calculate").disabled = false;
  document.getElementById("coloumError").style.visibility="hidden"; 
  document.getElementById("upload").disabled = true;
}