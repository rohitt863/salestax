//onClick Calculate button this function call and according tax call other function
function calculate(){
    let selectedTax=sessionStorage.getItem("selected");
    switch(selectedTax){
        case Constants.getVat()   : vatTax(selectedTax);
                      break;
        case Constants.getSales() : salesTax(selectedTax);
                      break;
        case Constants.getBoth()  :  allTax(selectedTax);
                      break;   
    }
    document.getElementById('selectd-value').style.display='flex'; 
}

//this function calculate sales tax
function salesTax(selectedTax){  
    let finalArray=JSON.parse(sessionStorage.getItem("dataArray"));
    let header=finalArray[0];
    let headerIndex= findIndexOfHeader(header);
    let len=header.length;
    header[len]=Constants.getSalesTaxPercentage();
    header[len+1]=Constants.getSalesTaxAmount();
    header[len+2]=Constants.getFinalPrice(); 
    let country=headerIndex[1]; 
    let cost=headerIndex[0];
    let outPutArray=finalArray.slice(1,finalArray.length);
    let taxStretegyObj=new TaxStretegy();
    outPutArray.forEach(function(item){
        let len=item.length;         
        let tax=taxStretegyObj.getTax(item[country],selectedTax);
        item[len]=tax;
        let taxAmountRes=findTaxAmount(item[cost],tax);
        if(isNaN(taxAmountRes)){
            taxAmountRes=Constants.getInvalidInput();
        }
        item[len+1]=taxAmountRes;
        let taxCalculation=calculateTax(item[cost],tax); 
        if(isNaN(taxCalculation)){
            taxCalculation=Constants.getInvalidInput();
        }
          item[len+2]=taxCalculation; 
    });
    destroyOutputTable();
    outPutHeader(header);    
    outPutTable(outPutArray);
}

//this function calculate vat tax
function vatTax(selectedTax){
    let finalArray=JSON.parse(sessionStorage.getItem("dataArray"));
    let header=finalArray[0];
    let headerIndex= findIndexOfHeader(header);
    let len=header.length;
    header[len]=Constants.getVatPercentage();
    header[len+1]=Constants.getVatAmount();
    header[len+2]=Constants.getFinalPrice(); 
    let country=headerIndex[1]; 
    let cost=headerIndex[0];
    let outPutArray=finalArray.slice(1,finalArray.length);
    let taxStretegyObj=new TaxStretegy();
    outPutArray.forEach(function(item){
        let len=item.length;         
        let tax=taxStretegyObj.getTax(item[country],selectedTax);
        item[len]=tax;
        let taxAmount=findTaxAmount(item[cost],tax);
        if(isNaN(taxAmount)){
            taxAmount=Constants.getInvalidInput();
        }
        item[len+1]=taxAmount;
        let taxCalculation=calculateTax(item[cost],tax);    
        if(isNaN(taxCalculation)){
            taxCalculation=Constants.getInvalidInput();
        }
        item[len+2]=taxCalculation;
    });
    destroyOutputTable();
    outPutHeader(header);     
    outPutTable(outPutArray);
}

//this function calculate all tax
function allTax(selectedTax){
    let finalArray=JSON.parse(sessionStorage.getItem("dataArray"));
    let header=finalArray[0];
    let headerIndex= findIndexOfHeader(header);
    let len=header.length;
    header[len]=Constants.getSalesTaxPercentage();
    header[len+1]=Constants.getSalesTaxAmount();
    header[len+2]=Constants.getVatPercentage();
    header[len+3]=Constants.getVatAmount();
    header[len+4]=Constants.getFinalPrice(); 
    let country=headerIndex[1]; 
    let cost=headerIndex[0];
    let outPutArray=finalArray.slice(1,finalArray.length);
    let taxStretegyObj=new TaxStretegy();
    outPutArray.forEach(function(item){
        let len=item.length; 
        let salesTaxResult=taxStretegyObj.getTax(item[country],Constants.getSales());  
        item[len]=salesTaxResult;
        let taxAmountSales=findTaxAmount(item[cost],salesTaxResult);
        if(isNaN(taxAmountSales)){
            taxAmountSales=Constants.getInvalidInput();
        }
        item[len+1]=taxAmountSales;
        let vatTaxResult=taxStretegyObj.getTax(item[country],Constants.getVat());
        item[len+2]=vatTaxResult;
        let taxAmoutVat=findTaxAmount(item[cost],vatTaxResult);
        if(isNaN(taxAmoutVat)){
            taxAmoutVat=Constants.getInvalidInput();
        }
        item[len+3]=taxAmoutVat;
        let taxCalculation=calculateAllTax(item[cost],salesTaxResult,vatTaxResult);     
        if(isNaN(taxCalculation)){
            taxCalculation=Constants.getInvalidInput();
        }
        item[len+4]=taxCalculation;
    });
    destroyOutputTable();
    outPutHeader(header);     
    outPutTable(outPutArray);
}
//this function  calculate tax amount using tax percentage and actual cost
function findTaxAmount(cost,tax){
    return (tax/100)*cost;
}
//this function find Cost and Country column index
function findIndexOfHeader(header){
    let indexHeader=[];
    let costIndex=0;
    let countryIndex=0;
    for(let index=0;index<header.length;index++){
        if(header[index].toLowerCase().trim()==Constants.getCost()){
            costIndex=index;
        }else if(header[index].toLowerCase().trim()==Constants.getCountry()){
            countryIndex=index;
        }
    }
    indexHeader[0]=costIndex;
    indexHeader[1]=countryIndex;
    return indexHeader;
}
//this function calculate all tax
function calculateAllTax(value,salesTaxResult,vatTaxResult){
    let sum=salesTaxResult+vatTaxResult;
    return parseInt(value)+parseInt(value)*sum/100; 
}
//this function calculate tax
function calculateTax(value,tax){
    return parseInt(value)+parseInt(value)*tax/100;
}
//onclick logout button this function call and logout the user    
function logout(){
    let confirmation=confirm(Constants.getAreYouSure());
    if(confirmation){
        sessionStorage.clear();
        localStorage.removeItem('userName');
        localStorage.removeItem('pass');
        fbSignOut();
        googleSignOut();
        window.location.href=Constants.getLoginPage();
    }
}
//if table exist then destroy output table
function destroyOutputTable(){
    if ($.fn.DataTable.isDataTable("#output-table")) {
       $("#output-table").DataTable().destroy();
       $('#output-table tbody').empty();
    }
}
//making output header
function outPutHeader(header){
    let output="";
    header.forEach(function(item){
      output+=`<th>${item}</th>`;
    });
    document.getElementById("output-header").innerHTML="";
    document.getElementById("output-header").innerHTML=output;
    console.log(header);
}
//disable calculate button
function outPutTable(outPutArray){
    outputDataTable(outPutArray);
    document.getElementById("calculate").disabled = true;
}
//making output table
function outputDataTable(outPutArray){
    $(document).ready(function(){
        $("#output-table").DataTable({
            data:outPutArray,
            paging:true,
            ordering:true,
            dom:'Bfrtrip',
            buttons:[
                {   
                    text:'Export CSV',
                    extend:'csv',
                    filename:'Output'
                }
            ]
        });
    });
}
//onclick drop down button this function call
function accordingTaxCalculateTax(){
    sessionStorage.setItem("selected",document.getElementById("selectd-value").value);
    calculate();
}