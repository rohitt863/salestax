﻿# Web Application for Tax Calculations

A simple web application that will help you to calculate taxes on the products according to the countries.
 
## Built with

 - [HTML](https://en.wikipedia.org/wiki/HTML5)
 - [CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets)
 - [Javascript](https://en.wikipedia.org/wiki/JavaScript)
 - [Bootstrap](https://en.wikipedia.org/wiki/Bootstrap_%28front-end_framework%29)

## Installation

 - [Clone this repository](https://gitlab.com/rohitt863/salestax.git)

## Usage

User have to make **CSV** file which contains **Product-Name, Cost and Country**.  After making the CSV file, the user has to upload the file in the **processingData.html** page, and select one of the tax from the drop-down menu and upload it. 
After uploading the file, uploaded file will be shown in a table. Below the table, there will be a **Calculate** button, when the user click these button another table will appear on the page.
At the **top-left** corner of the table, a button **Export CSV** will appear, through which the user can download the calculated data. 
