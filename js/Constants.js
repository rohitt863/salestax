const INDIA="india";
const US="us";
const JAPAN="japan";
const SALES="sales";
const VAT="vat";
const BOTH="both";
const ZERO=0;
const SALES_TAX_PERCENTAGE="SALES tax(%)"
const SALES_TAX_AMOUNT="SALES tax amount"
const FINAL_PRICE="Final price";
const INVALID_AMOUNT="Invalid input";
const VAT_PERCENTAGE="VAT tax(%)";
const VAT_AMOUNT="VAT tax amount";
const COST="cost";
const COUNTRY="country";
const ARE_YOU_SURE="Are you sure, you want to logout";
const LOGIN_PAGE="login.html";
const CHOOSE_CSV_FILE="Please choose a CSV file.";
const COMMA=",";
const CSV_EMPTY="CSV is empty please insert some data..";
const INVALID_EXTENSION="Invalid extention";
const THIS_BROWSER_NOT_SUPPORT="This browser does not support HTML5";
const COLUMN_NOT_SUPPORT="Price and Country column header must have names 'Cost and Country' respectively"
const HEADER_NOT_SUPPORTED="NOTE: The price column header should always have the name 'Cost' and country column header should always have the name 'Country'.Please fix it and RE-UPLOAD";
class Constants{

	static getIndia(){
		return INDIA;
	}
	static getUs(){
		return US;
	}
	static getJapan(){
		return JAPAN;
	}
	static getSales(){
		return SALES;
	}
	static getVat(){
		return VAT;
	}
	static getBoth(){
		return BOTH;
	}
	static getZero(){
		return ZERO;
	}
	static getSalesTaxPercentage(){
		return SALES_TAX_PERCENTAGE;
	}
	static getSalesTaxAmount(){
		return SALES_TAX_AMOUNT;
	}
	static getFinalPrice(){
		return FINAL_PRICE;
	}
	static getInvalidInput(){
		return INVALID_AMOUNT;
	}
	static getVatPercentage(){
		return VAT_PERCENTAGE;
	}
	static getVatAmount(){
		return VAT_AMOUNT;
	}
	static getCost(){
		return COST;
	}
	static getCountry(){
		return COUNTRY;
	}
	static getAreYouSure(){
		return ARE_YOU_SURE;
	}
	static getLoginPage(){
		return LOGIN_PAGE;
	}
	static getChooseCsvFile(){
		return CHOOSE_CSV_FILE;
	}
	static getComma(){
		return COMMA;
	}
	static getCsvEmpty(){
		return CSV_EMPTY;
	}
	static getInvalidExtension(){
		return INVALID_EXTENSION;
	}
	static getBrowserNotSupport(){
		return THIS_BROWSER_NOT_SUPPORT;
	}
	static getColumnNotSupport(){
		return COLUMN_NOT_SUPPORT;
	}
	static getHeaderNotSupported(){
		return HEADER_NOT_SUPPORTED;
	}
}