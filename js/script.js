function setdataIntoSession() {
    if(emailValidator() && mobileNumberValidator() && passwordValidator('password') && passwordConfirmCheck('password','confirm_password')){
        sessionStorage.setItem('email', document.getElementById("email").value );
        sessionStorage.setItem('mobile', document.getElementById("mobile").value );
        sessionStorage.setItem('password', document.getElementById("password").value );
        document.getElementById("submit-button").type="submit";
        document.getElementById("signup-form").action="login.html";
    }else{
        document.getElementById("submit-button").type="button";
    }
}

function authentication(){
	if(sessionStorage.password==document.getElementById("password").value && 
		(sessionStorage.mobile==document.getElementById("userName").value ||
		sessionStorage.email==document.getElementById("userName").value)){
        if(document.getElementById("chkId").value==1){
         localStorage.setItem('userName',document.getElementById("userName").value);
         localStorage.setItem('pass',document.getElementById("password").value);
        }
    sessionStorage.setItem('token', Math.random());
    window.location.href="../templates/processData.html";
	}else if(localStorage.pass==document.getElementById("password").value && 
        localStorage.userName==document.getElementById("userName").value){
        sessionStorage.setItem('token', Math.random());
        window.location.href="../templates/processData.html";
    }else{
		document.getElementById("error").innerHTML="<span><b>Invalid credentials</b></span>";
	}
}

function userValidity(){
    let userName=document.getElementById("userName").value;
    if(userName==sessionStorage.mobile || userName==sessionStorage.email){
        return true;
    }else{
        return false;
    }
}

function forgetPassword(){
    if(userValidity()){
        document.getElementById("forget_password_link").setAttribute("data-toggle", "modal");
    }else{
        document.getElementById("error").innerHTML="<b>User doesn't Exist!!</b>";
        document.getElementById("forget_password_link").removeAttribute("data-toggle");
    }
}

function saveNewPassword(){
    sessionStorage.setItem('password',document.getElementById("forgetPassword").value);
    localStorage.setItem('pass',document.getElementById("forgetPassword").value);
}
