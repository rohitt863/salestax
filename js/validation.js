function mobileNumberLengthLimit(event){
	let input=document.getElementById("mobile").value;
	if(input.length>9 || event.which==101){
		event.preventDefault();
	}
}

function alphabetsOnly(event){
	let input=String.fromCharCode(event.which);
	if(!(/[A-Za-z]/.test(input))){
		event.preventDefault();
	}
}

function emailValidator(){
	let email=document.getElementById("email").value;
	if(email.length==0){
		document.getElementById("email-requirement").innerHTML="*email required";
		return false;
	}else if(!email.match(/[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/)){
		document.getElementById("email-requirement").innerHTML="Invalid email";
		return false;
	}else{
		document.getElementById("email-requirement").innerHTML="";
		return true;
	}
}

function mobileNumberValidator(){
	let mobile=document.getElementById("mobile").value;
	if(mobile.length==0){
		document.getElementById("mobileNumber-requirement").innerHTML="*mobile number required";
		return false;
	}else if(mobile.length!=10){
		document.getElementById("mobileNumber-requirement").innerHTML="Invalid mobile number";
		return false;
	}else{
		document.getElementById("mobileNumber-requirement").innerHTML="";
		return true;
	}
}

function passwordValidator(passwordFieldId){
	let password=document.getElementById(passwordFieldId).value;
	if(password.length==0){
		document.getElementById("password-requirement").innerHTML="*password required";
		return false;
	}else if(!password.match(/[A-Z]/)){
		document.getElementById("password-requirement").innerHTML="*should contain atleast 1 uppercase letter";
		return false;
	}else if(!password.match(/[a-z]/)){
		document.getElementById("password-requirement").innerHTML="*should contain atleast 1 lowercase letter";
		return false;
	}else if(!password.match(/[0-9]/)){
		document.getElementById("password-requirement").innerHTML="*should contain atleast 1 number";
		return false;
	}else if(!password.match(/[!@#$%^&*]/)){
		document.getElementById("password-requirement").innerHTML="*should contain atleast 1 special character(e.g. !,@,#,$,%,^,&,*)";
		return false;
	}else if(!(8<password.length) || !(password.length<12) || ((!(8<password.length)) && (!(password.length<12)))){
		document.getElementById("password-requirement").innerHTML="*should be 8 to 12 characters long";
		return false;
	}else{
		document.getElementById("password-requirement").innerHTML="";
		return true;
	}
}

function passwordConfirmCheck(passwordFieldId,confirmPasswordFieldId) {
	let confirmPassword=document.getElementById(confirmPasswordFieldId).value;
	let password=document.getElementById(passwordFieldId).value;
	if(password==confirmPassword){
		document.getElementById("password_error").innerHTML="";
		return true;
	}else{
		document.getElementById("password_error").innerHTML="!!Password incorrect";
		return false;		
	}
}